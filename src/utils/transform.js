import moment from "moment";

export const getClubTimes = () => {
  const started = moment(new Date("09/01/2020"));
  const now = moment();
  const monthsDifDecimal = now.diff(new Date(started), "months", true);
  return {
    started: started.format("MMMM YYYY"),
    current: now.format("MMMM YYYY"),
    monthsDifDecimal,
    monthsDif: Math.ceil(monthsDifDecimal),
  };
};

export const transformUserData = (data) => {
  if (!data || !data?.treasury) {
    return data;
  }
  const time = getClubTimes();
  let statsData = [
    {
      statSubtitle: "Invested Months",
      statTitle: `${data.treasury.number_of_paid_months} / ${time.monthsDif}`,
      statIconName: "fas fa-calendar",
      statIconColor: "bg-blueGray-600 ",
    },
    {
      statSubtitle: "Invested Amount",
      statTitle: `${data.treasury.investment}`,
      statIconName: "fas fa-money-check-dollar",
      statIconColor: "bg-teal-500",
    },
    {
      statSubtitle: "Remaining Amount",
      statTitle: `${time.monthsDif * 1000 - data?.treasury?.investment}`,
      statIconName: "fas fa-cash-register",
      statIconColor: "bg-orange-500",
    },

    {
      statSubtitle: "Total Invested",
      statTitle: `${time.monthsDif * 1000}`,
      statIconName: "fas fa-money-bill",
      statIconColor: "bg-emerald-500",
    },
  ];

  if (data.role === "vendor") {
    if (data.value === "chit_20") {
      statsData = [
        {
          statSubtitle: "Invested Months",
          statTitle: `${data.treasury.number_of_paid_months} / 20`,
          statIconName: "fas fa-calendar",
          statIconColor: "bg-blueGray-600 ",
        },
        {
          statSubtitle: "Invested Amount",
          statTitle: `${data.treasury.investment}`,
          statIconName: "far fa-money-check-dollar",
          statIconColor: "bg-teal-500",
        },
        {
          statSubtitle: "Return Amount",
          statTitle: `${data?.treasury?.investment_return}`,
          statIconName: "fas fa-cash-register",
          statIconColor: "bg-orange-500",
        },
      ];
    } else {
      statsData = [
        {
          statSubtitle: "Invested Amount",
          statTitle: `${data.treasury.investment}`,
          statIconName: "far fa-chart-bar",
          statIconColor: "bg-teal-500",
        },
        {
          statSubtitle: "Return Amount",
          statTitle: `${data?.treasury?.investment_return}`,
          statIconName: "fas fa-chart-pie",
          statIconColor: "bg-orange-500",
        },
      ];
    }
  }

  return {
    ...data,
    ...time,
    statsData,
    member_holding_club_money: data.treasury.holding_amount || 0,
  };
};

export const transformClubData = (data) => {
  const time = getClubTimes();

  const statsData = [
    {
      statSubtitle: "Number of Months",
      statTitle: `${time.monthsDif || 0}`,
      statIconName: "fas fa-calendar",
      statIconColor: "bg-blueGray-600",
    },
    {
      statSubtitle: "Number of Members",
      statTitle: `${data.members.length || 0}`,
      statIconName: "fas fa-user",
      statIconColor: "bg-yellow-500",
    },
    {
      statSubtitle: "Members Paid",
      statTitle: `${data.club.received || 0}`,
      statIconName: "fas fa-credit-card",
      statIconColor: "bg-emerald-500",
    },
    {
      statSubtitle: "Members Remaining",
      statTitle: `${(time.monthsDif * (data.members.length || 0) * 1000) - (data.club.received || 0)}`,
      statIconName: "fas fa-chart-pie",
      statIconColor: "bg-orange-500",
    },
    {
      statSubtitle: "Invested",
      statTitle: `${data.club.investment || 0}`,
      statIconName: "fas fa-money-check-dollar",
      statIconColor: "bg-teal-500",
    },
    {
      statSubtitle: "Invested Returns",
      statTitle: `${data.club.investment_return || 0}`,
      statIconName: "fas fa-cash-register",
      statIconColor: "bg-pink-500",
    },
    {
      statSubtitle: "Liquidity",
      statTitle: `${data.club.holding_amount || 0}`,
      statIconName: "fa fa-money-bill",
      statIconColor: "bg-red-600",
    },
    {
      statSubtitle: "Expenditure",
      statTitle: `${data.club.spent_amount || 0}`,
      statIconName: "fas fa-balance-scale",
      statIconColor: "bg-lightBlue-500",
    },
  ];
  return {
    ...data,
    ...time,
    statsData,
  };
};
