export const requiredInputMsg = 'This input is required';
export const validNumberMsg = 'Enter a valid number';
export const numberMinMsg = 'Should be greater than 0';
export const numberMaxMsg = 'Should be leaser than 1000000';
export const numberBetweenMsg = 'Enter a valid number between 0 to 1000000';
export const notesMinMsg = 'Text is too short!';
export const notesMaxMsg = 'Text is too long!';
export const notesBetweenMsg = 'Enter a valid notes with length between 10 to 2000';
