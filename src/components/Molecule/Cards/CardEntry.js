import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { useRouter } from "next/router";
import { Controller, useForm } from "react-hook-form";
import DatePicker from "react-datepicker";
import {
  requiredInputMsg,
  validNumberMsg,
  numberMinMsg,
  numberMaxMsg,
  notesMinMsg,
  notesMaxMsg,
} from "../../../utils/message";
import { getMethod, postMethod } from "../../../utils/integration";

// components

export const COMMANDS_KEY = {
  transfer: "Transferred club between members",
  deposit: "Deposit member money",
  withdraw: "Withdraw member money",
  expenditure: "Add club expenditure",
  invest: "Add club money investment",
  return_on_invest: "Add club money investment returns",
};

export default function CardEntry() {
  const [isSubmitting, setSubmitting] = useState(false);
  const router = useRouter();
  const [isLoading, setLoading] = useState(true);
  const [userData, setUserData] = useState([]);
  const [vendorData, setVendorData] = useState([]);
  const [fromUserData, setFromUserData] = useState([]);
  const [toUserData, setToUserData] = useState([]);
  const [fromMsg, setFromMsg] = useState("");
  const [toMsg, setToMsg] = useState("");
  const {
    register,
    handleSubmit,
    watch,
    trigger,
    reset,
    control,
    setValue,
    formState: { errors },
  } = useForm();
  const watchMethod = watch("method", "transfer");

  const onSubmit = async (data) => {
    setSubmitting(true);
    if(!data.transactionOn || data.transactionOn === '') {
      data.transactionOn = new Date();
    }
    const {
      success,
      msg,
      data: resData,
    } = await postMethod("transaction", data);
    console.log({ success, msg, data });
    if (success) {
      getMethod("treasury/cleanCatch").then((catchData) => console.log(catchData));
      toast.success(`Transaction created! ID: ${resData?._id}`);
      reset();
    } else {
      setValue("password", "");
      if (msg === "invalid_password") {
        toast.error("Incorrect password!");
      }
      if (msg === "unsuccessful") {
        toast.error("Transaction creation was unsuccessful");
      }
      if (msg === "from_insufficient_balance") {
        toast.error(`Insufficient balance (${data.from})`);
      }
      if (msg === "to_insufficient_balance") {
        toast.error(`Insufficient balance (${data.to})`);
      }
      if (msg === "self_transfer") {
        toast.error("Self transfer are not allowed");
      }
    }
    setSubmitting(false);
    return;
  };

  const loadData = async () => {
    const { success, data } = await getMethod(`user/only`);
    if (success && data && data.length > 0) {
      setLoading(false);
      setUserData(data.filter((e) => e.role === "member"));
      setVendorData(data.filter((e) => e.role === "vendor"));
    } else {
      router.push("/");
    }
  };

  useEffect(() => {
    loadData();
  }, []);

  useEffect(() => {
    setValue("method", "transfer");
  }, [setValue, userData, vendorData]);

  useEffect(() => {
    if (!isLoading) {
      setValue("amount", 0);
    }
  }, [isLoading, setValue]);

  const fromToMsgHandler = (method) => {
    if (["deposit", "transfer"].includes(method)) {
      setFromMsg("Sender");
      setToMsg("Receiver");
    }
    if (method === "expenditure") {
      setFromMsg("Payer");
      setToMsg("");
    }
    if (method === "invest") {
      setFromMsg("Payer");
      setToMsg("Receiver");
    }

    if (method === "withdraw") {
      setFromMsg("Sender");
      setToMsg("Withdrawer");
    }
  };

  useEffect(() => {
    fromToMsgHandler(watchMethod);
    if (watchMethod === "deposit") {
      setValue("amount", 1000);
      trigger("amount");
    } else {
      setValue("amount", 0);
    }
    if (
      ["deposit", "withdraw", "transfer", "expenditure"].includes(watchMethod)
    ) {
      setFromUserData(userData);
      setToUserData(userData);
    }
    if (watchMethod === "invest") {
      setFromUserData(userData);
      setToUserData(vendorData.filter((e) => e.value === "chit_20"));
    }
    if (watchMethod === "return_on_invest") {
      setFromUserData(vendorData);
      setToUserData(userData);
    }
  }, [setValue, trigger, userData, vendorData, watchMethod]);

  useEffect(() => {
    if (fromUserData && fromUserData.length > 0) {
      setValue("from", fromUserData[0].value);
    }
    if (toUserData && toUserData.length > 0) {
      setValue("to", toUserData[0].value);
    }
  }, [fromUserData, setValue, toUserData, trigger]);

  return (
    <>
      <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-100 border-0">
        <div className="rounded-t bg-white mb-0 px-6 py-6">
          <div className="text-center flex justify-between">
            <h6 className="text-blueGray-700 text-xl font-bold">
              Create Transaction
            </h6>
            <button
              className={`font-bold uppercase text-xs px-4 py-2 rounded shadow outline-none focus:outline-none mr-1 ease-linear transition-all duration-150 ${
                isSubmitting
                  ? "bg-blueGray-100 active:bg-blueGray-100 text-black"
                  : "bg-blueGray-700 active:bg-blueGray-600 text-white"
              }`}
              type="submit"
              form="entry-form"
              disabled={isSubmitting}
            >
              {isSubmitting ? "Saving..." : "Save"}
            </button>
          </div>
        </div>
        <div className="flex-auto px-4 lg:px-10 py-10 pt-0">
          <form onSubmit={handleSubmit(onSubmit)} id="entry-form">
            <h6 className="text-blueGray-400 text-sm mt-3 mb-6 font-bold uppercase">
              Transaction Type
            </h6>
            <div className="flex flex-wrap">
              <div className="w-full lg:w-12/12 px-4">
                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    Method *
                  </label>
                  <select
                    name="method"
                    {...register("method", {
                      required: requiredInputMsg,
                    })}
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                  >
                    {Object.entries(COMMANDS_KEY).map(([key, val], i) => (
                      <option key={i} value={key}>
                        {val}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
            </div>

            <hr className="mt-6 border-b-1 border-blueGray-300" />

            <h6 className="text-blueGray-400 text-sm mt-3 mb-6 font-bold uppercase">
              Transaction Details
            </h6>
            <div className="flex flex-wrap">
              <div className="w-full lg:w-6/12 px-4">
                <div className="relative w-full mb-8">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    From *{" "}
                    <span className="text-xs font-light text-blueGray-400">
                      {" "}
                      {fromMsg ? `(${fromMsg})` : ""}
                    </span>
                  </label>
                  <select
                    name="from"
                    {...register("from", {
                      required: requiredInputMsg,
                    })}
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                  >
                    {fromUserData.sort((a, b) => (a.name > b.name) ? 1 : -1).map((e, i) => (
                      <option key={i} value={e.value}>
                        {e.name}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
              {watchMethod !== "expenditure" && (
                <div className="w-full lg:w-6/12 px-4">
                  <div className="relative w-full mb-3">
                    <label
                      className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                      htmlFor="grid-password"
                    >
                      To *{" "}
                      <span className="text-xs font-light text-blueGray-400">
                        {" "}
                        {toMsg ? `(${toMsg})` : ""}
                      </span>
                    </label>
                    <select
                      name="to"
                      {...register("to", {
                        required: requiredInputMsg,
                      })}
                      className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    >
                      {toUserData.sort((a, b) => (a.name > b.name) ? 1 : -1).map((e, i) => (
                        <option key={i} value={e.value}>
                          {e.name}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>
              )}

              <div
                className={`w-full lg:w-6/12 px-4 ${
                  watchMethod !== "expenditure" ? "lg:w-4/12" : "lg:w-6/12"
                }`}
              >
                <div className="relative w-full mb-8">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    Amount *
                  </label>
                  <input
                    type="number"
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    defaultValue=""
                    placeholder="Enter the transaction amount"
                    name="amount"
                    disabled={watchMethod === "deposit"}
                    onKeyPress={(e) => {
                      if (e.key === "e" || e.key === "-") {
                        e.preventDefault();
                      }
                    }}
                    {...register("amount", {
                      required: requiredInputMsg,
                      pattern: {
                        value: /^[0-9]+$/,
                        message: validNumberMsg,
                      },
                      min: {
                        value: 1,
                        message: numberMinMsg,
                      },
                      max: {
                        value: 1000000,
                        message: numberMaxMsg,
                      },
                    })}
                  />

                  {errors.amount ? (
                    <span className="text-sm text-red-600 error-code absolute -bottom-20 left-0">
                      {errors.amount.message}
                    </span>
                  ) : (
                    <></>
                  )}
                </div>
              </div>
              <div className="w-full lg:w-6/12 px-4">
                <div className="relative w-full mb-8">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    Transaction Date
                  </label>
                  <Controller
                    control={control}
                    name="transactionOn"
                    render={({ field }) => (
                      <DatePicker
                        className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                        placeholderText="Select date"
                        onChange={(date) => field.onChange(date)}
                        selected={field.value || new Date()}
                        dateFormat="d/MM/yyyy"
                      />
                    )}
                  />
                  {errors.transactionOn ? (
                    <span className="text-sm text-red-600 error-code absolute -bottom-20 left-0">
                      {errors.transactionOn.message}
                    </span>
                  ) : (
                    <></>
                  )}
                </div>
              </div>
            </div>

            <hr className="mt-6 border-b-1 border-blueGray-300" />

            <div className="flex flex-wrap mt-3">
              <div className="w-full lg:w-12/12 px-4">
                <div className="relative w-full mb-8">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    Note
                  </label>
                  <textarea
                    type="text"
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    rows="4"
                    defaultValue=""
                    placeholder="Notes on the transaction"
                    name="notes"
                    {...register("notes", {
                      minLength: {
                        value: 20,
                        message: notesMinMsg,
                      },
                      maxLength: {
                        value: 2000,
                        message: notesMaxMsg,
                      },
                    })}
                  ></textarea>

                  {errors.notes ? (
                    <span className="text-sm text-red-600 error-code absolute -bottom-20 left-0">
                      {errors.notes.message}
                    </span>
                  ) : (
                    <></>
                  )}
                </div>
              </div>
            </div>
            <div className="w-full lg:w-12/12 px-4">
              <div className="relative w-full mb-8">
                <label
                  className="block uppercase text-xs font-bold mb-2 text-blueGray-600"
                  htmlFor="grid-password"
                >
                  Password *
                </label>
                <input
                  type="text"
                  className={`border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150 ${
                    errors.password ? "error-input" : ""
                  }`}
                  defaultValue=""
                  placeholder="Enter the portal password"
                  name="password"
                  {...register("password", {
                    required: requiredInputMsg,
                  })}
                />
                {errors.password ? (
                  <span className="text-sm text-red-600 error-code absolute -bottom-20 left-0">
                    {errors.password.message}
                  </span>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}
