import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Loader from "../Loader";
import { getMethod } from "../../../utils/integration";
// components

const CardTransaction = ({
  title,
  limit = 6,
  page = 1,
  url,
  method,
  eMethod,
  role,
  user,
  sortBy,
  hideFrom,
  hideTo,
  hideMethod,
}) => {
  const router = useRouter();
  const [tableData, setTableData] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const [transURL, setTransUrl] = useState("/transaction/view?page=1&limit=20");

  const loadData = async () => {
    let GET_URL = `transaction?page=${page}&limit=${limit}`;
    let REDIRECT_URL = transURL;

    if (title) {
      REDIRECT_URL = `${REDIRECT_URL}&title=${title}`;
    }

    if (method) {
      GET_URL = `${GET_URL}&method=${method}`;
      REDIRECT_URL = `${REDIRECT_URL}&method=${method}`;
    }
    if (eMethod) {
      GET_URL = `${GET_URL}&eMethod=${eMethod}`;
      REDIRECT_URL = `${REDIRECT_URL}&eMethod=${eMethod}`;
    }
    if (role) {
      GET_URL = `${GET_URL}&role=${role}`;
      REDIRECT_URL = `${REDIRECT_URL}&role=${role}`;
    }
    if (sortBy) {
      GET_URL = `${GET_URL}&sortBy=${sortBy}`;
      REDIRECT_URL = `${REDIRECT_URL}&sortBy=${sortBy}`;
    }
    if (user) {
      GET_URL = `${GET_URL}&user=${user}`;
      REDIRECT_URL = `${REDIRECT_URL}&user=${user}`;
    }

    setTransUrl(REDIRECT_URL);
    const { success, data } = await getMethod(GET_URL);
    if (success) {
      setTableData(data.content);
      setLoading(false);
    } else {
      router.push("/");
    }
  };

  useEffect(() => {
    loadData();
  }, [limit, page, url, method, eMethod, role, user]);

  const handleClick = (e, href) => {
    e.preventDefault();
    router.push(href);
  };

  const handleDeleteClick = (e, transactionId) => {
    if(e.detail === 2) {
      router.push(`/add?transactionId=${transactionId}`);
    }
  };

  return (
    <>
      {isLoading ? (
        <Loader path="Transactions" />
      ) : (
        <>
          {tableData && tableData.length > 0 ? (
            <div className="relative flex flex-col min-w-0 break-words bg-white w-full shadow-lg rounded">
              <div className="rounded-t mb-0 px-4 py-3 border-0">
                <div className="flex flex-wrap items-center">
                  <div className="relative w-full px-4 max-w-full flex-grow flex-1">
                    <h3 className="font-semibold text-base text-blueGray-700">
                      {title}
                    </h3>
                  </div>
                  {tableData && tableData.length > 0 ? (
                    <>
                      <div className="relative w-full px-4 max-w-full flex-grow flex-1 text-right">
                        <button
                          className="bg-indigo-500 text-white disabled:bg-slate-50 hover:bg-indigo-800 active:bg-indigo-600 text-xs font-bold uppercase px-6 py-1 rounded outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 "
                          type="button"
                          onClick={(e) => handleClick(e, transURL)}
                        >
                          Sell All
                        </button>
                      </div>
                    </>
                  ) : (
                    <></>
                  )}
                </div>
              </div>
              <div className="block w-full overflow-x-auto">
                {/* Projects table */}
                <table className="items-center w-full bg-transparent border-collapse">
                  <thead className="thead-light">
                    <tr>
                      <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                        Transactions ID
                      </th>

                      {!hideFrom && (
                        <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                          From
                        </th>
                      )}
                      <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                        Amount
                      </th>
                      {!hideMethod && (
                        <th className=" px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                          Method
                        </th>
                      )}
                      {!hideTo && (
                        <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                          To
                        </th>
                      )}
                      <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                        Date
                      </th>
                      <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                        Added
                      </th>
                      <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-center">
                        Delete
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {tableData && tableData.length > 0 ? (
                      tableData.map((item, i) => (
                        <tr key={i}>
                          <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                            {item._id}
                          </td>

                          {!hideFrom && (
                            <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
                              {item.from.name}
                            </th>
                          )}
                          <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                            {item.amount}
                          </td>
                          {!hideMethod && (
                            <th className="uppercase border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
                              {item.methodShow}
                            </th>
                          )}
                          {!hideTo && (
                            <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
                              {item.to?.name ? item.to?.name : "Unknown"}
                            </th>
                          )}
                          <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                            {item?.transactionDay}
                          </td>
                          <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                            {item?.createdDay}
                          </td>
                          <td className="text-center border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                            <i className="fas fa-trash cursor-pointer text-blueGray-500" onClick={(e) => handleDeleteClick(e, item._id)}></i>
                          </td>
                        </tr>
                      ))
                    ) : (
                      <tr>
                        <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                          <p className="p-4 w-12/12 test-center">
                            No records found!
                          </p>
                        </td>
                      </tr>
                    )}
                  </tbody>
                </table>
              </div>
            </div>
          ) : (
            <></>
          )}
        </>
      )}
    </>
  );
};

export default CardTransaction;
