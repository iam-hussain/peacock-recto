import React from "react";
import Image from "next/image";
import Loader from "../Loader";
// components

const CardTreasury = ({ data, monthsDif, isLoading, isVendor, title }) => {

  return (
    <>
      {isLoading ? (
        <Loader path="Transactions" />
      ) : (
        <>
          {data && data.length > 0 ? (
            <div className="relative flex flex-col min-w-0 break-words bg-white w-full shadow-lg rounded">
              <div className="rounded-t mb-0 px-4 py-3 border-0">
                <div className="flex flex-wrap items-center">
                  <div className="relative w-full px-4 max-w-full flex-grow flex-1">
                    <h3 className="font-semibold text-base text-blueGray-700">
                      {title}
                    </h3>
                  </div>
                </div>
              </div>
              <div className="block w-full overflow-x-auto">
                {/* Projects table */}
                <table className="items-center w-full bg-transparent border-collapse">
                  <thead className="thead-light">
                    <tr>
                      <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                        Name
                      </th>
                      {!isVendor ? (
                        <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                          Months
                        </th>
                      ) : (
                        <></>
                      )}
                      <th className=" px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                        {isVendor ? "Spent" : "Invested"}
                      </th>
                      <th className=" px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                        {isVendor ? "Returns" : "Remaining"}
                      </th>
                      <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                        {isVendor ? "Profit" : "Holding"}
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {data && data.length > 0 ? (
                      data.map((item, i) => (
                        <tr key={i}>
                          <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left flex items-center image-text">
                            {isVendor ? (
                              <>{item?.user?.name}</>
                            ) : (
                              <>
                                <Image
                                  src={`/image/${item?.user?.image}`}
                                  alt={item?.user?.name}
                                  className="rounded-full small-image"
                                  objectFit="cover"
                                  quality={100}
                                  height={50}
                                  width={50}
                                />{" "}
                                <span className="ml-2 font-bold text-black">
                                  {item?.user?.name}
                                </span>
                              </>
                            )}
                          </th>
                          {isVendor ? (
                            <></>
                          ) : (
                            <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
                              {item.number_of_paid_months}
                            </th>
                          )}

                          <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                            {item.investment}
                          </td>
                          <th className="uppercase border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
                            {isVendor
                              ? item.investment_return
                              : monthsDif * 1000 - item.investment}
                          </th>
                          <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
                            {isVendor
                              ? item.investment_return - item.investment
                              : item.holding_amount}
                          </th>
                        </tr>
                      ))
                    ) : (
                      <tr>
                        <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                          <p className="p-4 w-12/12 test-center">
                            No records found!
                          </p>
                        </td>
                      </tr>
                    )}
                  </tbody>
                </table>
              </div>
            </div>
          ) : (
            <></>
          )}
        </>
      )}
    </>
  );
};

export default CardTreasury;
