import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { requiredInputMsg } from "../../../utils/message";
import { getMethod, postMethod } from "../../../utils/integration";
import { toast } from "react-toastify";

// components

export default function CardDeleteTransaction({ transactionId }) {
  const [isSubmitting, setSubmitting] = useState(false);
  const {
    register,
    handleSubmit,
    setValue,
    reset,
    formState: { errors },
  } = useForm();

  const onSubmit = async (data) => {
    setSubmitting(true);
    const { success, msg } = await postMethod("transaction/delete", data);
    if (success) {
      getMethod("treasury/cleanCatch").then((catchData) => console.log(catchData));
      toast.success("Transaction deleted!");
      reset();
    } else {
      setValue("password", "");
      if (msg === "invalid_password") {
        toast.error("Incorrect password!");
      }
      if (msg === "unsuccessful") {
        toast.error("Transaction ID not found!");
      }
    }
    setSubmitting(false);
    return;
  };

  useEffect(() => {
    if (transactionId && transactionId !== "") {
      setValue("transactionId", transactionId);
    }
  }, [setValue, transactionId]);

  return (
    <>
      <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-100 border-0">
        <div className="rounded-t bg-white mb-0 px-6 py-6">
          <div className="text-center flex justify-between">
            <h6 className="text-blueGray-700 text-xl font-bold">
              Delete Transaction
            </h6>
            <button
              className={`font-bold uppercase text-xs px-4 py-2 rounded shadow outline-none focus:outline-none mr-1 ease-linear transition-all duration-150 ${
                isSubmitting
                  ? "bg-blueGray-100 active:bg-blueGray-100 text-black"
                  : "bg-blueGray-700 active:bg-blueGray-600 text-white"
              }`}
              type="submit"
              form="delete-form"
              disabled={isSubmitting}
            >
            {isSubmitting ? "Deleting..." : "Delete"}
            </button>
          </div>
        </div>
        <div className="flex-auto px-4 lg:px-10 py-10 pt-0">
          <form onSubmit={handleSubmit(onSubmit)} id="delete-form">
            <div className="flex flex-wrap mt-4">
              <div className="w-full lg:w-12/12 px-4">
                <div className="relative w-full mb-8">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    Transaction ID *
                  </label>
                  <input
                    type="text"
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    defaultValue=""
                    placeholder="Enter the transaction ID"
                    name="transactionId"
                    {...register("transactionId", {
                      required: requiredInputMsg,
                    })}
                  />
                  {errors.transactionId ? (
                    <span className="text-sm text-red-600 error-code absolute -bottom-20 left-0">
                      {errors.transactionId.message}
                    </span>
                  ) : (
                    <></>
                  )}
                </div>
              </div>
              <div className="w-full lg:w-12/12 px-4">
                <div className="relative w-full mb-8">
                  <label
                    className="block uppercase text-xs font-bold mb-2 text-blueGray-600"
                    htmlFor="grid-password"
                  >
                    Password *
                  </label>
                  <input
                    type="text"
                    className={`border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150 ${
                      errors.password ? "error-input" : ""
                    }`}
                    defaultValue=""
                    placeholder="Enter the portal password"
                    name="password"
                    {...register("password", {
                      required: requiredInputMsg,
                    })}
                  />
                  {errors.password ? (
                    <span className="text-sm text-red-600 error-code absolute -bottom-20 left-0">
                      {errors.password.message}
                    </span>
                  ) : (
                    <></>
                  )}
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}
