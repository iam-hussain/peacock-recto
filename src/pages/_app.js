import React from "react";
// import ReactDOM from "react-dom";
import App from "next/app";
import Head from "next/head";
import { ToastContainer } from "react-toastify";

import "react-toastify/dist/ReactToastify.css";

import "@fortawesome/fontawesome-free/css/all.min.css";
import "react-datepicker/dist/react-datepicker.css";
import "../styles/tailwind.css";
import "../styles/global.css";

export default class MyApp extends App {
  static async getInitialProps({ Component, router, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }
  render() {
    const { Component, pageProps } = this.props;

    const Layout = Component.layout || (({ children }) => <>{children}</>);

    return (
      <React.Fragment>
        <Head>
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
          />
          <title>Peacock Club</title>
        </Head>
        <Layout>
          <Component {...pageProps} />
          <ToastContainer
            hideProgressBar
            position="bottom-right"
            autoClose={8000}
          />
        </Layout>
      </React.Fragment>
    );
  }
}
