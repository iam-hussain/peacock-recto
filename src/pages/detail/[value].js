import React, { useEffect, useState } from "react";
import Image from "next/image";
import { useRouter } from "next/router";
import { getMethod } from "../../utils/integration";

import BoardLayout from "../../components/Layouts/Board";
import Loader from "../../components/Molecule/Loader";
import CardTransaction from "../../components/Molecule/Cards/CardTransaction";

import Stats from "../../components/Molecule/Stats";

import { transformUserData } from "../../utils/transform";

const Details = () => {
  const router = useRouter();
  const { value } = router.query;

  const [userData, setUserData] = useState({});
  const [isLoading, setLoading] = useState(true);

  const loadData = async () => {
    const { success, data } = await getMethod(`user/details/${value}`);
    if (success) {
      console.log({ ud: transformUserData(data) });
      setUserData(transformUserData(data));
      setLoading(false);
    } else {
      router.push("/");
    }
  };

  useEffect(() => {
    loadData();
  }, [value]);

  return (
    <>
      {isLoading ? (
        <Loader />
      ) : (
        <>
          <div className="flex flex-wrap mt-16 lg:mt-20 mb-8">
            <div className="w-full xl:w-12/12  lg:px-4">
              <div className="relative flex flex-col min-w-0 break-words bg-blueGray-800 w-full  shadow-xl rounded-lg ">
                <div className="px-6">
                  {userData && userData.role === "member" ? (
                    <div className="flex flex-wrap justify-center">
                      <div className="w-full lg:w-3/12 px-4 lg:order-2 flex justify-center relative">
                        <div className="relative">
                          <div className="round-image shadow-xl rounded-full h-auto align-middle border-none absolute -m-16 -ml-20 lg:-ml-16 max-w-150-px">
                            <Image
                              alt={userData?.name}
                              src={`/image/${userData?.image}`}
                              className="rounded-full"
                              objectFit="cover"
                              quality={100}
                              height={150}
                              width={150}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  ) : (
                    <></>
                  )}

                  <div
                    className={`text-center ${
                      userData && userData.role === "member" ? "mt-20" : "mt-4"
                    }`}
                  >
                    <h3 className="text-4xl font-semibold leading-normal mb-8 text-white mb-2">
                      {userData?.name}
                    </h3>
                  </div>
                  {userData?.statsData && (
                    <Stats statsData={userData.statsData} />
                  )}

                  {userData?.joinedDay ||
                  userData?.member_holding_club_money ? (
                    <>
                      <div className="mt-4 py-2 border-t border-blueGray-200 text-center">
                        <div className="flex justify-center py-4 lg:pt-4 pt-8 flex-wrap ">
                          {userData?.member_holding_club_money ? (
                            <div className="mr-4 p-3 text-center">
                              <span className="text-xl font-bold block uppercase tracking-wide text-white">
                                {userData.member_holding_club_money}
                              </span>
                              <span className="text-sm text-blueGray-400">
                                Holding Club Amount
                              </span>
                            </div>
                          ) : <></>}
                          {userData?.member_holding_club_money ? (
                            <div className="mr-4 p-3 text-center">
                              <span className="text-xl font-bold block uppercase tracking-wide text-white">
                                {userData.joinedDay}
                              </span>
                              <span className="text-sm text-blueGray-400">
                                Joined on Club
                              </span>
                            </div>
                          ): <></>}
                        </div>
                      </div>
                    </>
                  ) : (
                    <></>
                  )}
                </div>
              </div>
            </div>
          </div>
        </>
      )}
      {userData && userData.role === "member" ? (
        <>
          <div className="flex flex-wrap mb-8">
            <div className="w-full xl:w-12/12">
              {isLoading ? (
                <Loader />
              ) : (
                <CardTransaction
                  title="Invested"
                  url={`transaction?`}
                  method={"deposit"}
                  user={userData._id}
                  role="from"
                />
              )}
            </div>
          </div>
          <div className="flex flex-wrap mb-8">
            <div className="w-full xl:w-12/12">
              {isLoading ? (
                <Loader />
              ) : (
                <CardTransaction
                  title="Received"
                  url={`transaction?`}
                  user={userData._id}
                  role="to"
                />
              )}
            </div>
          </div>
          <div className="flex flex-wrap mb-8">
            <div className="w-full xl:w-12/12">
              {isLoading ? (
                <Loader />
              ) : (
                <CardTransaction
                  title="Sent"
                  url={`transaction?`}
                  user={userData._id}
                  role="from"
                  eMethod={"deposit"}
                />
              )}
            </div>
          </div>
        </>
      ) : (
        <>
          <div className="flex flex-wrap mb-8">
            <div className="w-full xl:w-12/12">
              {isLoading ? (
                <Loader />
              ) : (
                <CardTransaction
                  title="Sent"
                  url={`transaction?`}
                  user={userData._id}
                  role="to"
                />
              )}
            </div>
          </div>
          <div className="flex flex-wrap mb-8">
            <div className="w-full xl:w-12/12">
              {isLoading ? (
                <Loader />
              ) : (
                <CardTransaction
                  title="Received"
                  url={`transaction?`}
                  user={userData._id}
                  role="from"
                />
              )}
            </div>
          </div>
        </>
      )}
    </>
  );
};

Details.layout = BoardLayout;

export default Details;
